// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.StringProcessors.Html
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class AddHeaderExtensionBase : ReplacementExtensionBase
    {
        /// <summary>
        /// Gets or sets the header markup.
        /// </summary> 
        protected string HeaderMarkup { get; set; }

        /// <summary>
        /// Determines whether this instance can remove the tag from the specified <see cref="StringBuilder" />.
        /// </summary>
        /// <param name="view">The <see cref="StringBuilder" /> to check against.</param>
        /// <returns>
        /// Always returns true unless overridden.
        /// </returns>
        protected override bool CanRemoveTag(StringBuilder view)
        {
            return view.ToString().IndexOf("</head>", StringComparison.OrdinalIgnoreCase) != -1;
        }

        /// <summary>Processes the html markup.</summary>
        /// <param name="view">The <see cref="StringBuilder"/> to be processed.</param>
        /// <param name="arguments">The name value pairs of arguments to be passed into the processors.</param>
        protected override void ProcessMarkup(StringBuilder view, IDictionary<string, object> arguments) 
          {
            this.startPos = view.ToString().IndexOf("</head>", StringComparison.OrdinalIgnoreCase);
            if (this.startPos == -1)
            {
                return;
            }

            view.Insert(this.startPos, string.Format(this.HeaderMarkup, this.argumentString));
        }
    }
}
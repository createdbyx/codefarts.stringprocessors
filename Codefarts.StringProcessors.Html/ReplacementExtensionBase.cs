// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.StringProcessors.Html
{
    using System.Collections.Generic;

    using Codefarts.StringProcessors;
    using System.Text;

    /// <summary>
    /// Provides a base class for performing string replacements.
    /// </summary>
    public abstract class ReplacementExtensionBase : IProcessor
    {
        /// <summary>Gets or sets the name of the tag.</summary>
        /// <value>The name of the tag.</value>
        public string TagName { get; set; }

        /// <summary>Used to hold a list of arguments that may have been</summary>
        protected string argumentString;

        protected int startPos;
        public string BeginString { get; set; }
        public string EndString { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="ReplacementExtensionBase"/> class.
        /// </summary>
        protected ReplacementExtensionBase()
        {
            this.BeginString = "[";
            this.EndString = "]";
        }

        /// <summary>Processes the html markup.</summary>
        /// <param name="view">The <see cref="StringBuilder"/> to be processed.</param>
        /// <param name="arguments">The name value pairs of arguments to be passed into the processors.</param>
        protected abstract void ProcessMarkup(StringBuilder view, IDictionary<string, object> arguments);

        protected bool TryToGetTagArgs(StringBuilder view)
        {
            // find start tag
            var section = view.ToString();
            this.startPos = section.IndexOf(string.Format(this.BeginString + "{0}", this.TagName), System.StringComparison.Ordinal);
            if (this.startPos == -1)
            {
                return false;
            }

            // find end tag
            var endPos = section.IndexOf(this.EndString, this.startPos + this.BeginString.Length + this.TagName.Length, System.StringComparison.Ordinal);
            if (endPos == -1)
            {
                return false;
            }

            // get parameter
            this.argumentString = section.Substring(this.startPos + this.TagName.Length + this.BeginString.Length, endPos - this.startPos - (this.TagName.Length + this.BeginString.Length)).Trim();

            if (this.CanRemoveTag(view))
            {
                // remove the tag
                view.Remove(this.startPos, endPos + this.EndString.Length - this.startPos);
            }

            return true;
        }

        /// <summary>Determines whether this instance can remove the tag from the specified <see cref="StringBuilder"/>.</summary>
        /// <param name="view">The <see cref="StringBuilder"/> to check against.</param>
        /// <returns>Always returns true unless overridden.</returns>
        protected virtual bool CanRemoveTag(StringBuilder view)
        {
            return true;
        }

        /// <summary>Executes string processing.</summary>
        /// <param name="view">The <see cref="StringBuilder"/> to be processed.</param>
        /// <param name="arguments">The name value pairs of arguments to be passed into the processors.</param>
        public virtual void Execute(StringBuilder view, IDictionary<string, object> arguments)
        {
            this.startPos = -1;
            this.argumentString = string.Empty;
            if (!this.TryToGetTagArgs(view))
            {
                return;
            }

            this.ProcessMarkup(view, arguments);
        }

        /// <summary>Gets the priority of this processor.</summary>
        /// <remarks>
        /// Priorities are used to determine what order string processors should be executed. Lower priority values are processed first, higher priority values last.
        /// </remarks>
        public virtual int Priority { get; private set; }
    }
}
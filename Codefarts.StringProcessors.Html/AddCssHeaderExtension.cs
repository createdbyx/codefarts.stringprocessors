// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.StringProcessors.Html
{
  using Codefarts.StringProcessors;
  using System.ComponentModel.Composition;

     [Export(typeof(IProcessor))]
    public class AddCssHeaderExtension : AddHeaderExtensionBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddCssHeaderExtension"/> class.
        /// </summary>
        public AddCssHeaderExtension()
        {
            this.TagName = "addheader css";
            this.HeaderMarkup = "<link href=\"{0}\" rel=\"stylesheet\" type=\"text/css\" />\r\n";
        }

        /// <summary>
        /// Gets the priority of this processor.
        /// </summary>
        /// <remarks>
        /// Priorities are used to determine what order string processors should be executed. Lower priority values are processed first, higher priority values last.
        /// </remarks>
        public override int Priority
        {
            get
            {
                return 1;
            }
        }
    }
}
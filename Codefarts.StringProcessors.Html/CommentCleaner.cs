// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.StringProcessors.Html
{
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Text;
    using Codefarts.StringProcessors;

    [Export(typeof(IProcessor))]
    public class CommentCleaner : IProcessor
    {
        /// <summary>Executes string processing.</summary>
        /// <param name="view">The <see cref="StringBuilder"/> to be processed.</param>
        /// <param name="arguments">The name value pairs of arguments to be passed into the processors.</param>
        public virtual void Execute(StringBuilder view, IDictionary<string, object> arguments)
        {
            //ERR: If there are html comments nested within comments this method fails
            var html = view.ToString();

            var tagName = "<!--";
            var endTagName = "-->";
            var startPos = html.IndexOf(tagName);
            while (startPos != -1)
            {
                var index = startPos + tagName.Length;
                var endIndex = html.IndexOf("-->", index, System.StringComparison.Ordinal);
                if (endIndex == -1)
                {
                    startPos = html.IndexOf(tagName, startPos + tagName.Length); ;
                    continue;
                }

                if (!string.IsNullOrWhiteSpace(html.Substring(index, endIndex - index)))
                {
                    startPos = html.IndexOf(tagName, endIndex + endTagName.Length);
                    continue;
                }

                view.Remove(startPos, endIndex + endTagName.Length - startPos);
                html = view.ToString();
            }
        }

        /// <summary>Gets the priority of this processor.</summary>
        /// <remarks>
        /// Priorities are used to determine what order string processors should be executed. Lower priority values are processed first, higher priority values last.
        /// </remarks>
        public int Priority
        {
            get
            {
                return int.MinValue;
            }
        }
    }
}
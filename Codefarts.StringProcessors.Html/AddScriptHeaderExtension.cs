// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.StringProcessors.Html
{
    using System.ComponentModel.Composition;
    using Codefarts.StringProcessors;

    [Export(typeof(IProcessor))]
    public class AddScriptHeaderExtension : AddHeaderExtensionBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddScriptHeaderExtension"/> class.
        /// </summary>
        public AddScriptHeaderExtension()
        {
            this.TagName = "addheader script";
            this.HeaderMarkup = "<script type=\"text/javascript\" src=\"{0}\"></script>\r\n";
        }
    }
}
﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

using System.Text;

namespace Codefarts.StringProcessors.Html.Tests
{
    using System.Collections.Generic;

    public class ReplacementBaseTestClass : ReplacementExtensionBase
    {
        private int priority;

        public ReplacementBaseTestClass()
        {
            this.TagName = "template";
        }

        protected override void ProcessMarkup(StringBuilder view, IDictionary<string, object> arguments)
        {
            view.Insert(this.startPos, string.Format(this.ContentToInsert, this.argumentString));
        }

        public new int Priority
        {
            get { return this.priority; }
            set { this.priority = value; }
        }   

        public string ContentToInsert { get; set; }
    }
}

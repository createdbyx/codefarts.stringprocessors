﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

using System.Text;

namespace Codefarts.StringProcessors.Html.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class ReplacementExtensionBaseTests
    {
        [TestMethod]
        public void NoArguments()
        {
            var model = new ReplacementBaseTestClass();
            model.ContentToInsert = "test";

            var data = new StringBuilder("12345[template]67890");
            data.ApplyProcessors(new[] { model }, 1000);

            Assert.AreEqual("12345test67890", data.ToString());
        }

        [TestMethod]
        public void OneArguments()
        {
            var model = new ReplacementBaseTestClass();
            model.ContentToInsert = "test";

            var data = new StringBuilder("12345[template Arg1]67890");
            data.ApplyProcessors(new[] { model }, 1000);

            Assert.AreEqual("12345test67890", data.ToString());
        }

        [TestMethod]
        public void DoubleBracketContainers()
        {
            var model = new ReplacementBaseTestClass();
            model.ContentToInsert = "test";

            model.BeginString = "[[";
            model.EndString = "]]";

            var data = new StringBuilder("12345[[template]]67890");
            data.ApplyProcessors(new[] { model }, 1000);

            Assert.AreEqual("12345test67890", data.ToString());
        }

        [TestMethod]
        public void HtmlCommentContainers()
        {
            var model = new ReplacementBaseTestClass();
            model.ContentToInsert = "test";

            model.BeginString = "<!--";
            model.EndString = "-->";

            var data = new StringBuilder("12345<!--template-->67890");
            data.ApplyProcessors(new[] { model }, 1000);

            Assert.AreEqual("12345test67890", data.ToString());
        }

        [TestMethod]
        public void DoubleBracketFrontSingleBracketBack()
        {
            var model = new ReplacementBaseTestClass();
            model.ContentToInsert = "test";

            model.BeginString = "[[";

            var data = new StringBuilder("12345[[template]67890");
            data.ApplyProcessors(new[] { model }, 1000);

            Assert.AreEqual("12345test67890", data.ToString());
        }

        [TestMethod]
        public void SingleBracketFrontDoubleBracketBack()
        {
            var model = new ReplacementBaseTestClass();
            model.ContentToInsert = "test";

            model.EndString = "]]";

            var data = new StringBuilder("12345[template]]67890");
            data.ApplyProcessors(new[] { model }, 1000);

            Assert.AreEqual("12345test67890", data.ToString());
        }

        [TestMethod]
        public void DuplicateEndBracket()
        {
            var model = new ReplacementBaseTestClass();
            model.ContentToInsert = "test";

            var data = new StringBuilder("12345[template]]67890");
            data.ApplyProcessors(new[] { model }, 1000);

            Assert.AreEqual("12345test]67890", data.ToString());
        }

        [TestMethod]
        public void DuplicateStartBracket()
        {
            var model = new ReplacementBaseTestClass();
            model.ContentToInsert = "test";

            var data = new StringBuilder("12345[[template]67890");
            data.ApplyProcessors(new[] { model }, 1000);

            Assert.AreEqual("12345[test67890", data.ToString());
        }

        [TestMethod]
        public void DuplicateStartAndEndBrackets()
        {
            var model = new ReplacementBaseTestClass();
            model.ContentToInsert = "test";

            var data = new StringBuilder("12345[[template]]67890");
            data.ApplyProcessors(new[] { model }, 1000);

            Assert.AreEqual("12345[test]67890", data.ToString());
        }

        [TestMethod]
        public void DuplicateStartAndEndBracketsWithTwoArguments()
        {
            var model = new ReplacementBaseTestClass();
            model.ContentToInsert = "test";

            var data = new StringBuilder("12345[[template arg1 arg2]]67890");
            data.ApplyProcessors(new[] { model }, 1000);

            Assert.AreEqual("12345[test]67890", data.ToString());
        }

        [TestMethod]
        public void EmbededTags()
        {
            var model = new ReplacementBaseTestClass();
            model.ContentToInsert = "test";

            var data = new StringBuilder("12345[template arg1 [template arg2]]67890");
            data.ApplyProcessors(new[] { model }, 1000);

            Assert.AreEqual("12345test]67890", data.ToString());
        }
    }
}

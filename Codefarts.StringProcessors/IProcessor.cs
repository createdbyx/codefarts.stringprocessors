﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.StringProcessors
{
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Provides a interface for processing <see cref="StringBuilder"/> data.
    /// </summary>
    public interface IProcessor
    {
        /// <summary>
        /// Executes string processing.
        /// </summary>
        /// <param name="data">A reference to a <see cref="StringBuilder"/> that will be processed.</param>
        /// <param name="arguments">The name value pairs of arguments to be passed into the processors.</param>
        void Execute(StringBuilder data, IDictionary<string, object> arguments);

        /// <summary>
        /// Gets the priority of this processor.
        /// </summary>
        /// <remarks>Priorities are used to determine what order string processors should be executed. Lower priority values are processed first, higher priority values last.</remarks>
        int Priority { get; }
    }    
}

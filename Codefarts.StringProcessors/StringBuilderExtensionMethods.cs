﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.StringProcessors
{
    using System.Text;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Provides extension methods for the <see cref="StringBuilder"/> type.
    /// </summary>
    public static class StringBuilderExtensionMethods
    {
        /// <summary>
        /// Applies string processors to a <see cref="StringBuilder"/> type.
        /// </summary>
        /// <param name="data">The <see cref="StringBuilder"/> to be processed.</param>
        /// <remarks>Restricted to 1000 processor execution loops.</remarks>  
        public static void ApplyProcessors(this StringBuilder data)
        {
            ApplyProcessors(data, null, StringProcessorRepository.Instance.Processors.ToArray(), 1000);
        }

        /// <summary>
        /// Applies string processors to a <see cref="StringBuilder"/> type.
        /// </summary>
        /// <param name="data">The <see cref="StringBuilder"/> to be processed.</param>
        /// <param name="arguments">The name value pairs of arguments to be passed into the processors.</param>
        /// <remarks>Restricted to 1000 processor execution loops.</remarks>  
        public static void ApplyProcessors(this StringBuilder data, IDictionary<string, object> arguments)
        {
            ApplyProcessors(data, StringProcessorRepository.Instance.Processors.ToArray(), 1000);
        }

        /// <summary>
        /// Applies string processors to a <see cref="StringBuilder"/> type.
        /// </summary>
        /// <param name="data">The <see cref="StringBuilder"/> to be processed.</param>
        /// <param name="processors">A collection of <see cref="IProcessor"/> implementations that will be used to process the string data.</param>
        /// <param name="maxIterations">An optional value that controls the maximum iterations the <see cref="ApplyProcessors(System.Text.StringBuilder)"/> 
        /// method will be invoked. Is restrict to a maximum of 1000.</param>
        public static void ApplyProcessors(this StringBuilder data, IProcessor[] processors, int maxIterations)
        {
            ApplyProcessors(data, null, processors, maxIterations);
        }

        /// <summary>
        /// Applies string processors to a <see cref="StringBuilder"/> type.
        /// </summary>
        /// <param name="data">The <see cref="StringBuilder"/> to be processed.</param>
        /// <param name="arguments">The name value pairs of arguments to be passed into the processors.</param>
        /// <param name="processors">A collection of <see cref="IProcessor"/> implementations that will be used to process the string data.</param>
        /// <param name="maxIterations">An optional value that controls the maximum iterations the <see cref="ApplyProcessors(System.Text.StringBuilder)"/> 
        /// method will be invoked. Is restrict to a maximum of 1000.</param>
        public static void ApplyProcessors(this StringBuilder data, IDictionary<string, object> arguments, IProcessor[] processors, int maxIterations)
        {
            var count = 0;
            // invoke the method and provide a validation callback that monitors how many times the validation callback
            // was invoked.
            ApplyProcessors(data, arguments, processors, sb =>
            {
                count++;
                // infinite recursion check / Restrict to "maxIterations" processor execution loops
                return count < Math.Min(maxIterations, 1000);
            });
        }

        /// <summary>
        /// Applies string processors to a <see cref="StringBuilder"/> type.
        /// </summary>
        /// <param name="data">The <see cref="StringBuilder"/> to be processed.</param>
        /// <param name="processors">A collection of <see cref="IProcessor"/> implementations that will be used to process the string data.</param>
        /// <param name="validate">Provides a callback that will be invoked after the processors have been applied but before the 
        /// <see cref="ApplyProcessors(System.Text.StringBuilder)"/> method recursively calls itself.</param>
        /// <remarks><p>The "validate" callback can be useful for validating or check for possible infinite recursion looping.</p>
        /// <p><see cref="ApplyProcessors(System.Text.StringBuilder)"/> only recursively calls itself if it detects any changes were made
        /// after executing all of the processors.</p></remarks>
        public static void ApplyProcessors(this StringBuilder data, IProcessor[] processors, Func<StringBuilder, bool> validate)
        {
            ApplyProcessors(data, null, processors, validate);
        }

        /// <summary>
        /// Applies string processors to a <see cref="StringBuilder"/> type.
        /// </summary>
        /// <param name="data">The <see cref="StringBuilder"/> to be processed.</param>
        /// <param name="arguments">The name value pairs of arguments to be passed into the processors.</param>
        /// <param name="processors">A collection of <see cref="IProcessor"/> implementations that will be used to process the string data.</param>
        /// <param name="validate">Provides a callback that will be invoked after the processors have been applied but before the 
        /// <see cref="ApplyProcessors(System.Text.StringBuilder)"/> method recursively calls itself.</param>
        /// <remarks><p>The "validate" callback can be useful for validating or check for possible infinite recursion looping.</p>
        /// <p><see cref="ApplyProcessors(System.Text.StringBuilder)"/> only recursively calls itself if it detects any changes were made
        /// after executing all of the processors.</p></remarks>
        public static void ApplyProcessors(this StringBuilder data, IDictionary<string, object> arguments, IProcessor[] processors, Func<StringBuilder, bool> validate)
        {
            // if null argument just return
            if (data == null || processors == null)
            {
                return;
            }

            // save original data
            var originalData = data.ToString();

            // run string processors  
            foreach (var item in processors)
            {
                item.Execute(data, arguments);
            }

            // invoke the validate method if available
            if (validate != null && !validate(data))
            {
                return;
            }

            // detect changes and if there were some reprocess data again
            if (originalData != data.ToString())
            {
                ApplyProcessors(data, processors, validate);
            }
        }         
    }
}

﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.StringProcessors
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Provides a repository for <see cref="IProcessor"/> implementations.
    /// </summary>
    public class StringProcessorRepository
    {
        /// <summary>
        /// Holds a reference to a <see cref="StringProcessorRepository"/> singleton.
        /// </summary>
        private static StringProcessorRepository singleton;

        /// <summary>
        /// Holds a list of registered <see cref="IProcessor"/> implementations.
        /// </summary>
        private readonly List<IProcessor> stringProcessors;

        /// <summary>
        /// Registers a <see cref="IProcessor"/> implementation.
        /// </summary>
        /// <param name="processor">The processor to be registered.</param>
        /// <exception cref="ArgumentNullException">If "processor" parameter is null.</exception>
        public void Register(IProcessor processor)
        {
            if (processor == null)
            {
                throw new ArgumentNullException("processor");
            }

            // determine where to insert processor into the list based on priority
            var index = 0;
            while (index < this.stringProcessors.Count && processor.Priority > this.stringProcessors[index].Priority)
            {
                index++;
            }

            this.stringProcessors.Insert(index, processor);
        }

        /// <summary>
        /// Gets a enumerable collection of registered string processors.
        /// </summary>
        public IEnumerable<IProcessor> Processors
        {
            get
            {
                return this.stringProcessors;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StringProcessorRepository"/> class.
        /// </summary>
        public StringProcessorRepository()
        {
            this.stringProcessors = new List<IProcessor>();
        }

        /// <summary>
        /// Gets a singleton instance of a <see cref="StringProcessorRepository"/> type.
        /// </summary>
        public static StringProcessorRepository Instance
        {
            get
            {
                return singleton ?? (singleton = new StringProcessorRepository());
            }
        }
    }
}